import { Component, OnInit } from '@angular/core';
import { Advertisement } from '../advertisement';
import { getEmptyAdvertisement } from '../helpers/getEmptyAdvertisement';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent {
  ads: Array<Advertisement>;

  constructor() {
    this.ads = [getEmptyAdvertisement()];
  }

  deleteAll() { this.ads = []; }
  deleteItem(index) { this.ads.splice(index, 1); }
  addItem() { this.ads.push(getEmptyAdvertisement()); }
  addItemFromTemplate() {}
}
