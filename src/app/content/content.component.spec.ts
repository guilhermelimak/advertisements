import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { getEmptyAdvertisement } from '../helpers/getEmptyAdvertisement';

import { ContentComponent } from './content.component';

describe('ContentComponent', () => {
  let component: ContentComponent;
  let fixture: ComponentFixture<ContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      declarations: [
        ContentComponent,
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize with one empty advertisement', () => {
    expect(component.ads[0]).toEqual(getEmptyAdvertisement());
  });

  it('should delete all advertisements when calling deleteAll method', () => {
    for (let i = 0; i < 9; i++) {
      component.ads.push(getEmptyAdvertisement());
    }

    expect(component.ads.length).toBe(10);
    component.deleteAll();
    expect(component.ads.length).toBe(0);
  });

  it('should delete a specific advertisement when calling deleteItem method', () => {
    component.ads = [];

    for (let i = 0; i < 9; i++) {
      component.ads.push(getEmptyAdvertisement());
      component.ads[i].coins = i;
    }

    const index = 3;

    expect(component.ads.length).toBe(9);
    component.deleteItem(index);
    expect(component.ads.length).toBe(8);
    expect(component.ads[index].coins).toBe(index + 1);
  });

  it('should add a new item when calling addItem', () => {
    const len = component.ads.length;

    component.addItem();
    expect(component.ads[len]).toEqual(getEmptyAdvertisement());
  });
});
