import { Component, Input } from '@angular/core';
import { Advertisement } from '../advertisement';
import { getEmptyAdvertisement } from '../helpers/getEmptyAdvertisement';

@Component({
  selector: 'app-advertisement-info',
  templateUrl: './advertisement-info.component.html',
  styleUrls: ['./advertisement-info.component.scss']
})

export class AdvertisementInfoComponent {
  placementOptions: Array<Object>;
  @Input() ad: Advertisement;

  constructor() {
    this.placementOptions = [
      { id: 0, name: `Before block 1`},
      { id: 1, name: `After block 1`},
      { id: 2, name: `Before block 2`},
      { id: 3, name: `After block 2`},
    ];
  }

  toggleOptinButtons() {
    this.ad[`optinButtons`] = !this.ad[`optinButtons`];
  }
}
