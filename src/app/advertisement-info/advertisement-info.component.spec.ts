import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AdvertisementInfoComponent } from './advertisement-info.component';
import { getEmptyAdvertisement } from '../helpers/getEmptyAdvertisement';

describe('AdvertisementInfoComponent', () => {
  let component: AdvertisementInfoComponent;
  let fixture: ComponentFixture<AdvertisementInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      declarations: [
        AdvertisementInfoComponent
      ],
      imports: [
        FormsModule,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvertisementInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should toggle optinButtons property when calling toggleOptinButtons', () => {
    component.ad = getEmptyAdvertisement();
    const oldVal = component.ad.optinButtons;
    component.toggleOptinButtons();
    expect(component.ad.optinButtons).toBe(!oldVal);
  });
});
