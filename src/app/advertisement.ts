export interface Advertisement {
  link: string;
  coins: number;
  imgUrl: string;
  placement: number;
  skipCooldown: number;
  optinButtons: boolean;
}
