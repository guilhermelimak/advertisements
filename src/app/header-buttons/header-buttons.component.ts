import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-header-buttons',
  templateUrl: './header-buttons.component.html',
  styleUrls: ['./header-buttons.component.scss']
})
export class HeaderButtonsComponent {
  @Output() onAddItem = new EventEmitter();
  @Output() onAddItemFromTemplate = new EventEmitter();
  @Output() onDeleteAll = new EventEmitter();
}
