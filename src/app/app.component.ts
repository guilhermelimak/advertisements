import { OnInit, ViewChild, Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  @ViewChild('sidenav') sidenav;
  toggleSidemenu: any;

  ngOnInit() {
    this.toggleSidemenu = () => this.sidenav.toggle(true);
  }
}
