import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Advertisement } from '../advertisement';

@Component({
  selector: 'app-advertisement-preview',
  templateUrl: './advertisement-preview.component.html',
  styleUrls: ['./advertisement-preview.component.scss']
})
export class AdvertisementPreviewComponent {
  @Input() ad: Advertisement;
  @Output() onDelete = new EventEmitter();
}
