export const getEmptyAdvertisement = () => ({
  link: ``,
  coins: 0,
  imgUrl: '',
  placement: 0,
  skipCooldown: 3,
  optinButtons: false,
});
