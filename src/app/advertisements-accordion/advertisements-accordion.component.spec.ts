import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {SqueezeBoxModule} from 'squeezebox';

import { AdvertisementsAccordionComponent } from './advertisements-accordion.component';

describe('AdvertisementsAccordionComponent', () => {
  let component: AdvertisementsAccordionComponent;
  let fixture: ComponentFixture<AdvertisementsAccordionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      declarations: [ AdvertisementsAccordionComponent ],
      imports: [SqueezeBoxModule],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvertisementsAccordionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit onDelete event when emitDelete is called', (done) => {
    const index = 2;

    component.onDelete.subscribe((x) => {
      expect(x).toBe(index);
      done();
    });

    component.emitDelete(index);
  });

  it('should emit onAdd event when emitAdd is called', (done) => {
    component.onAdd.subscribe((x) => {
      expect(x).toBeUndefined();
      done();
    });

    component.emitAdd();
  });
});
