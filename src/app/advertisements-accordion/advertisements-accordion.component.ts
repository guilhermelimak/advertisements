import { Component, Output, Input, EventEmitter } from '@angular/core';
import { Advertisement } from '../advertisement';

@Component({
  selector: 'app-advertisements-accordion',
  templateUrl: './advertisements-accordion.component.html',
  styleUrls: ['./advertisements-accordion.component.scss']
})
export class AdvertisementsAccordionComponent {
  @Input() ads: Array<Advertisement>;
  @Output() onDelete = new EventEmitter();
  @Output() onAdd = new EventEmitter();

  emitDelete(index) {
    this.onDelete.emit(index);
  }

  emitAdd() {
    this.onAdd.emit();
  }
}
