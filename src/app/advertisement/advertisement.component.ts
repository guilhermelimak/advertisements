import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Advertisement } from '../advertisement';

@Component({
  selector: 'app-advertisement',
  templateUrl: './advertisement.component.html',
  styleUrls: ['./advertisement.component.scss']
})
export class AdvertisementComponent {
  @Input() ad: Advertisement;
  @Input() index: number;
  @Output() onDelete =  new EventEmitter();

  emitDelete() { this.onDelete.emit(this.index); };
}
