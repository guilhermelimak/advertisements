import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {SqueezeBoxModule} from 'squeezebox';
import { MaterialModule } from '@angular/material';

import { AppComponent } from './app.component';
import { TopBarComponent } from './top-bar/top-bar.component';
import { ContentComponent } from './content/content.component';
import { BottomBarComponent } from './bottom-bar/bottom-bar.component';
import { HeaderTextComponent } from './header-text/header-text.component';
import { HeaderButtonsComponent } from './header-buttons/header-buttons.component';
import { AdvertisementComponent } from './advertisement/advertisement.component';
import { AdvertisementInfoComponent } from './advertisement-info/advertisement-info.component';
import { AdvertisementPreviewComponent } from './advertisement-preview/advertisement-preview.component';
import { AdvertisementsAccordionComponent } from './advertisements-accordion/advertisements-accordion.component';

import 'hammerjs';

@NgModule({
  declarations: [
    AppComponent,
    TopBarComponent,
    ContentComponent,
    BottomBarComponent,
    HeaderTextComponent,
    HeaderButtonsComponent,
    AdvertisementComponent,
    AdvertisementInfoComponent,
    AdvertisementPreviewComponent,
    AdvertisementsAccordionComponent
  ],
  imports: [
    BrowserModule,
    SqueezeBoxModule,
    FormsModule,
    HttpModule,
    MaterialModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
