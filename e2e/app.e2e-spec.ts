import { AdvertisingsPage } from './app.po';

describe('advertisings App', () => {
  let page: AdvertisingsPage;

  beforeEach(() => {
    page = new AdvertisingsPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
