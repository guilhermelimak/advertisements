# Advertisings[![build status](https://gitlab.com/guilhermelimak/advertisements/badges/master/build.svg)](https://gitlab.com/guilhermelimak/advertisements/commits/master)

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.0.0-rc.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive/pipe/service/class/module`.

## Build

Run `yarn build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `yarn test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Live demo

This project is hosted in: [Advertisements](https://advertisements-a4122.firebaseapp.com/).
(The name of the project causes it to be flagged by Adblocker extensions, which can cause the page assets requests to be blocked, so if any problem occur try deactivating your adblocker extension)

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
